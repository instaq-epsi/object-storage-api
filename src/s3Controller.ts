import * as AWS from "aws-sdk";

const ENDPOINT_S3 = process.env.ENDPOINT_S3 || "";
const ACCESS_KEY_S3 = process.env.ACCESS_KEY_S3 || "";
const SECRET_KEY_S3 = process.env.SECRET_KEY_S3 || "";
const REGION_S3 = process.env.REGION_S3 || "";

interface FilesResponse {
  message: string;
  data: any[];
}

interface CreatedFileResponse {
  fileUrl: string;
}

const client = new AWS.S3({
  endpoint: ENDPOINT_S3,
  accessKeyId: ACCESS_KEY_S3,
  secretAccessKey: SECRET_KEY_S3,
  region: REGION_S3
});

export const createFile = (
  bucketName: string,
  fileName: string,
  file: any
): Promise<CreatedFileResponse> => {
  const buf = new Buffer(
    file.replace(/^data:image\/\w+;base64,/, ""),
    "base64"
  );
  const contentType = file.replace(/^data:/, "").replace(/;base64.*/, "");
  return new Promise((resolve, reject) => {
    let params = {
      Bucket: bucketName,
      Key: fileName,
      Body: buf,
      ACL: "public-read",
      ContentEncoding: "base64",
      ContentType: contentType
    };
    client.upload(
      params,
      { partSize: 5 * 1024 * 1024, queueSize: 10 },
      (err: any, data: any) => {
        if (!err) {
          resolve({
            fileUrl: data.Location
          });
        } else {
          reject({
            error: err
          });
        }
      }
    );
  });
};
