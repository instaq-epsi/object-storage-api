import { ApolloServer } from "apollo-server-fastify";
import fastify from "fastify";
import { typeDefs } from "./typedefs";
import { resolvers } from "./resolvers";
import { context } from "./context";

const port = Number(process.env.PORT || "3000");
const addr = process.env.ADDR || "0.0.0.0";

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context
});

const app = fastify();

(async () => {
  app.register(server.createHandler());
  await app.listen(port, addr);
})();
