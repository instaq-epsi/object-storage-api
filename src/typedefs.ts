import { gql } from "apollo-server-fastify";

export const typeDefs = gql`
  type Query {
    image: Image
  }

  type Image {
    userId: String!
    image: String!
    url_thumbnail: String!
    url: String!
    created_at: String!
  }

  type Mutation {
    uploadImage(userId: String!, image: String!): Image
  }
`;
