const vision = require("@google-cloud/vision");
// Creates a client
const client = new vision.ImageAnnotatorClient();

export const start = async (base64: string, filename: string) => {
  const [result] = await client.safeSearchDetection({
    image: { content: base64 }
  });
  const detections = result.safeSearchAnnotation;

  if (
    detections.racy !== "VERY_LIKELY"
  ) {
    return true;
  }
  return false;
};
