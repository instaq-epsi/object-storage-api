import { Context, UserInputError, ApolloError } from "apollo-server-core";
import { GraphQLResolveInfo } from "graphql";
import { createFile } from "./s3Controller";
import { start } from "./cloudVision";
const uuidv4 = require("uuid/v4");

const imageThumbnail = require("image-thumbnail");

const bucket = process.env.BUCKET || "";

const THUMB_PREFIX = "thumb_";
const PICTURE_PREFIX = "picture_";

type Args = {
  userId: string;
  image: string;
};

export const resolvers = {
  Mutation: {
    uploadImage: async (
      _: any,
      args: Args,
      ctx: Context,
      info: GraphQLResolveInfo
    ) => {
      const response = {
        userId: args.userId,
        image: args.image,
        url_thumbnail: "",
        url: "",
        created_at: new Date()
      };
      const fileName = uuidv4();
      const base64 = args.image.replace(/^data:image\/\w+;base64,/, "");
      if (await start(base64, fileName)) {
        response.url = (
          await createFile(bucket, PICTURE_PREFIX + fileName, args.image)
        ).fileUrl;
        try {
          const contentType = args.image
            .replace(/^data:/, "")
            .replace(/;base64.*/, "");
          const options = { width: 50, height: 50, responseType: "base64" };
          const base64Thumbnail = `data:${contentType};base64,${await imageThumbnail(
            base64,
            options
          )}`;
          response.url_thumbnail = (
            await createFile(bucket, THUMB_PREFIX + fileName, base64Thumbnail)
          ).fileUrl;
        } catch (err) {
          throw new ApolloError(`Server error: ${err}`);
        }
        return response;
      }
      throw new UserInputError("Picture not explicit");
    }
  }
};
